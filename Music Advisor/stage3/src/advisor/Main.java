package advisor;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Scanner;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;

public class Main {
    
    private static Client client;
    
    public static void main(String[] args) throws IOException, InterruptedException {
        Scanner scanner = new Scanner(System.in);
        
        String spotifySite = "https://accounts.spotify.com";
        if (args.length >= 2 && args[0].equals("-access")) {
            spotifySite = args[1];
        }
        client = new Client(spotifySite);
    
        String input = null;
        boolean isAuthorized = false;
        
        while (true) {
//            input = input == null ? "auth" : scanner.nextLine();
            input = scanner.nextLine();
            if (isAuthorized) {
                switch (input) {
                    case "new":
                        System.out.println("---NEW RELEASES---\n" +
                                "Mountains [Sia, Diplo, Labrinth]\n" +
                                "Runaway [Lil Peep]\n" +
                                "The Greatest Show [Panic! At The Disco]\n" +
                                "All Out Life [Slipknot]");
                        break;
                    case "featured":
                        System.out.println("---FEATURED---\n" +
                                "Mellow Morning\n" +
                                "Wake Up and Smell the Coffee\n" +
                                "Monday Motivation\n" +
                                "Songs to Sing in the Shower");
                        break;
                    case "categories":
                        System.out.println("---CATEGORIES---\n" +
                                "Top Lists\n" +
                                "Pop\n" +
                                "Mood\n" +
                                "Latin");
                        break;
                    case "playlists":
                        System.out.println("--" + input.split(" ")[1].toUpperCase() + " PLAYLISTS---\n" +
                                "Walk Like A Badass  \n" +
                                "Rage Beats  \n" +
                                "Arab Mood Booster  \n" +
                                "Sunday Stroll");
                        break;
                    case "exit":
                        System.out.println("---GOODBYE!---");
                        return;
                }
            } else {
                switch (input) {
                    case "auth":
                        String code = getAuthCode();
                        System.out.println("code received");
//                        System.out.println(code);
                        System.out.println("making http request for access_token...");
                        // just prints json:
                        client.requestToken(code);
                        isAuthorized = true;
                        System.out.println("---SUCCESS---");
                        break;
                    case "exit":
                        System.out.println("---GOODBYE!---");
                        System.exit(0);
                    default:
                        System.out.println("Please, provide access for application.");
                        break;
                }
            }
        }
    }
    
    private static String getAuthCode() throws IOException, InterruptedException {
        AccessCodeServer server = new AccessCodeServer();
        try {
            server.start();
            client.askAuthorization();
            System.out.println("waiting for code...");
            return server.waitAccessCode();
        } finally {
            server.shutdown();
        }
    }
}

class AccessCodeServer {
    private int port = 8080;
    private HttpServer httpServer;
    private BlockingDeque<String> accessCodeQueue = new LinkedBlockingDeque<>();
    
    
    private void sendResponse(HttpExchange exchange, String response) throws IOException {
        exchange.sendResponseHeaders(200, response.length());
        exchange.getResponseBody().write(response.getBytes());
        exchange.getResponseBody().close();
    }
    
    void start() throws IOException {
        httpServer = HttpServer.create();
        httpServer.bind(new InetSocketAddress(port), 0);
        httpServer.createContext("/callback", exchange -> {
            try {
                String query = exchange.getRequestURI().getQuery();
                if (query == null || !query.contains("code=")) {
                    sendResponse(exchange, "Not found authorization code. Try again.");
                    return;
                }
                String accessCode = exchange.getRequestURI().getQuery().split("code=")[1];
                accessCodeQueue.add(accessCode);
                
                sendResponse(exchange, "Got the code. Return back to your program.");
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        
        httpServer.createContext("/hello", exchange -> {
            String hello = "hello, world";
            exchange.sendResponseHeaders(200, hello.length());
            exchange.getResponseBody().write(hello.getBytes());
            exchange.getResponseBody().close();
        });
    
        accessCodeQueue.clear();
        httpServer.start();
    }
    
    void shutdown() {
        httpServer.stop(1);
        accessCodeQueue.clear();
    }
    
    String waitAccessCode() throws InterruptedException {
        String code = accessCodeQueue.take();
        return code;
    }
}

/**
 * Used to work with http requests.
 */
class Client {
    private String tokenUri;
    private String authorizeUri;
    
    private static String client_id = "9792bed9428a40a1b79411f854062720";
    private static String client_secret = "48d899ccb2b144dfa1802ca2c7350557";
    
    private static String redirect_uri = "http://localhost:8080/callback";
    
    
    public Client(String spotifySite) {
         tokenUri = spotifySite + "/api/token";
          authorizeUri = spotifySite + "/authorize?client_id="
                + client_id + "&response_type=code&redirect_uri=" + redirect_uri;
    }
    
    void askAuthorization() {
        System.out.println("use this link to request the access code:\n" + authorizeUri);
    }
    
    void requestToken(String authorizationCode) throws IOException, InterruptedException {
        String body = "grant_type=authorization_code"
                + "&code=" + authorizationCode
                + "&redirect_uri=" + redirect_uri
                + "&client_id=" + client_id
                + "&client_secret=" + client_secret;
        
        HttpClient client = HttpClient.newBuilder().build();
        HttpRequest request = HttpRequest.newBuilder()
                .header("Content-Type", "application/x-www-form-urlencoded")
                .uri(URI.create(tokenUri))
                .POST(HttpRequest.BodyPublishers.ofString(body))
                .build();
        
        HttpResponse<?> response = client.send(request, HttpResponse.BodyHandlers.ofString());
        System.out.println("response:");
        System.out.println(response.body());
    }
}
