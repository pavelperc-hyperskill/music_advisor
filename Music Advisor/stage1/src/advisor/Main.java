// user solution:
// https://stepik.org/submissions/625271/139624294
package advisor;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {
        
        printWelcome();
        
        List<String> list = Arrays.asList("featured", "new", "categories", "playlists Mood");
        Scanner scanner = new Scanner(System.in);
        
        String readInput = scanner.nextLine();
        while (!readInput.equals("exit")) {
            if (list.contains(readInput)) {
                switch (readInput) {
                    case "new":
                        getNewReleases();
                        break;
                    case "featured":
                        getFeatured();
                        break;
                    case "categories":
                        getCategories();
                        break;
                    case "playlists Mood":
                        getPlaylists("Mood");
                        break;
                    default:
                        printError();
                }
            }
            
            readInput = scanner.nextLine();
        }
        
        System.out.println("---GOODBYE!---");
    }
    
    static void printWelcome() {
        System.out.println("Welcome to Music Advisor! Please enter a command:");
    }
    
    static void getNewReleases() {
        System.out.println("" +
                "---NEW RELEASES---\n" +
                "Mountains [Sia, Diplo, Labrinth]\n" +
                "Runaway [Lil Peep]\n" +
                "The Greatest Show [Panic! At The Disco]\n" +
                "All Out Life [Slipknot]");
    }
    
    static void getFeatured() {
        System.out.println("" +
                "---FEATURED---\n" +
                "Mellow Morning\n" +
                "Wake Up and Smell the Coffee\n" +
                "Monday Motivation\n" +
                "Songs to Sing in the Shower");
    }
    
    static void getCategories() {
        System.out.println("" +
                "---CATEGORIES---\n" +
                "Top Lists\n" +
                "Pop\n" +
                "Mood\n" +
                "Latin");
    }
    
    static void getPlaylists(String categoryName) {
        System.out.println("" +
                "---" + categoryName.toUpperCase() + " PLAYLISTS---\n" +
                "Walk Like A Badass  \n" +
                "Rage Beats  \n" +
                "Arab Mood Booster  \n" +
                "Sunday Stroll");
    }
    
    static void printError() {
        System.out.println("Command not found - please try again.");
    }
}
