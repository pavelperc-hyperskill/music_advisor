package advisor;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.*;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.stream.Collectors;


public class Main {
    
    private static AuthClient authClient;
    private static ApiClient apiClient;
    
    private static boolean isAuthorized() {
        return apiClient != null;
    }
    
    private static List<String> currentResults = null;
    private static int pageNum = 0;
    private static int pageSize = 5;
    
    
    private static void showResults() {
        if (currentResults == null || currentResults.isEmpty()) {
            return;
        }
        if (pageNum * pageSize >= currentResults.size() || pageNum < 0) {
            System.out.println("No more pages.");
            if (pageNum <= 0) {
                pageNum = 0;
            } else {
                pageNum = (currentResults.size() - 1) / pageSize;
            }
            return;
        }
        for (int i = pageNum * pageSize; i < (pageNum + 1) * pageSize; i++) {
            if (i < currentResults.size()) {
                System.out.println(currentResults.get(i));
            }
        }
        System.out.println("---PAGE " + (pageNum + 1) + " OF " + ((currentResults.size() - 1) / pageSize + 1) + "---");
    }
    
    
    public static void main(String[] args) throws Exception {
        Scanner scanner = new Scanner(System.in);
        
        String spotifyAccountSite = "https://accounts.spotify.com";
        String spotifyApiSite = "https://api.spotify.com";
        for (int i = 0; i < args.length - 1; i += 2) {
            if (args[i].equals("-access")) {
                spotifyAccountSite = args[i + 1];
            } else if (args[i].equals("-resource")) {
                spotifyApiSite = args[i + 1];
            } else if (args[i].equals("-page")) {
                try {
                    pageSize = Math.max(Integer.parseInt(args[i + 1]), 1);
                } catch (Exception ignored) {
                }
            }
        }
        authClient = new AuthClient(spotifyAccountSite);
        
        String input = null;
        
        while (true) {
//            input = input == null ? "auth" : scanner.nextLine();
            input = scanner.nextLine();
            if (isAuthorized()) {
                switch (input) {
                    case "new":
                        currentResults = apiClient.getNewReleases();
                        pageNum = 0;
                        showResults();
                        break;
                    case "featured":
                        currentResults = apiClient.getFeaturedPlaylists();
                        pageNum = 0;
                        showResults();
                        break;
                    case "categories":
                        currentResults = apiClient.getCategories();
                        pageNum = 0;
                        showResults();
                        break;
                    case "next":
                        pageNum++;
                        showResults();
                        break;
                    case "prev":
                        pageNum--;
                        showResults();
                        break;
                    case "exit":
                        return;
                    default:
                        if (input.startsWith("playlists ")) {
                            String category = input.substring("playlists ".length());
                            currentResults = apiClient.getPlaylists(category);
                            pageNum = 0;
                            showResults();
                        }
                }
            } else {
                switch (input) {
                    case "auth":
                        String authCode = getAuthCode();
                        System.out.println("code received");
                        System.out.println("making http request for access_token...");
                        String accessToken = authClient.requestToken(authCode);
                        apiClient = new ApiClient(accessToken, spotifyApiSite);
                        
                        System.out.println("Success!");
                        break;
                    case "exit":
                        System.out.println("---GOODBYE!---");
                        System.exit(0);
                    default:
                        System.out.println("Please, provide access for application.");
                        break;
                }
            }
        }
    }
    
    private static String getAuthCode() throws IOException, InterruptedException {
        AccessCodeServer server = new AccessCodeServer();
        try {
            server.start();
            authClient.askAuthorization();
            System.out.println("waiting for code...");
            return server.waitAccessCode();
        } finally {
            server.shutdown();
        }
    }
}

class AccessCodeServer {
    private int port = 8080;
    private HttpServer httpServer;
    private BlockingDeque<String> accessCodeQueue = new LinkedBlockingDeque<>();
    
    
    private void sendResponse(HttpExchange exchange, String response) throws IOException {
        exchange.sendResponseHeaders(200, response.length());
        exchange.getResponseBody().write(response.getBytes());
        exchange.getResponseBody().close();
    }
    
    void start() throws IOException {
        httpServer = HttpServer.create();
        httpServer.bind(new InetSocketAddress(port), 0);
        httpServer.createContext("/callback", exchange -> {
            try {
                String query = exchange.getRequestURI().getQuery();
                if (query == null || !query.contains("code=")) {
                    sendResponse(exchange, "Not found authorization code. Try again.");
                    return;
                }
                String accessCode = exchange.getRequestURI().getQuery().split("code=")[1];
                accessCodeQueue.add(accessCode);
                
                sendResponse(exchange, "Got the code. Return back to your program.");
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        
        httpServer.createContext("/hello", exchange -> {
            String hello = "hello, world";
            exchange.sendResponseHeaders(200, hello.length());
            exchange.getResponseBody().write(hello.getBytes());
            exchange.getResponseBody().close();
        });
        
        accessCodeQueue.clear();
        httpServer.start();
    }
    
    void shutdown() {
        httpServer.stop(1);
        accessCodeQueue.clear();
    }
    
    String waitAccessCode() throws InterruptedException {
        String code = accessCodeQueue.take();
        return code;
    }
}

class ApiClient {
    
    private String accessToken;
    private String spotifyApiSite;
    private final HttpClient client;
    
    private Map<String, String> cachedCategoriesToIds = new LinkedHashMap<>();
    
    public ApiClient(String accessToken, String spotifyApiSite) {
        this.accessToken = accessToken;
        this.spotifyApiSite = spotifyApiSite;
        
        client = HttpClient.newBuilder().build();
    }
    
    private HttpRequest.Builder requestBuilder(String path) {
        return HttpRequest.newBuilder()
                .header("Authorization", "Bearer " + accessToken)
                .uri(URI.create(spotifyApiSite + path));
    }
    
    private JsonObject getRequest(String path) throws IOException, InterruptedException {
        HttpRequest request = requestBuilder(path)
                .GET()
                .build();
        
        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
        return JsonParser.parseString(response.body()).getAsJsonObject();
    }
    
    
    List<String> getNewReleases() {
        try {
            JsonObject jo = getRequest("/v1/browse/new-releases");
            if (jo.has("error")) {
                System.out.println(jo.getAsJsonObject("error").get("message").getAsString());
                return null;
            }
            List<JsonObject> albums = new ArrayList<>();
            jo.getAsJsonObject("albums")
                    .getAsJsonArray("items")
                    .forEach(je -> albums.add(je.getAsJsonObject()));
            
            List<String> results = new ArrayList<>();
            for (JsonObject album : albums) {
                String name = album.get("name").getAsString();
                String url = album.getAsJsonObject("external_urls").get("spotify").getAsString();
                List<JsonObject> artists = new ArrayList<>();
                album.getAsJsonArray("artists").forEach(je -> artists.add(je.getAsJsonObject()));
                String artistNames = artists.stream()
                        .map(art -> art.get("name").getAsString())
                        .collect(Collectors.joining(", ", "[", "]"));
                
                results.add(name + "\n" + artistNames + "\n" + url + "\n");
            }
            return results;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    List<String> getFeaturedPlaylists() {
        try {
            JsonObject jo = getRequest("/v1/browse/featured-playlists");
            return parsePlaylists(jo);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    private List<String> parsePlaylists(JsonObject jo) {
        if (jo.has("error")) {
            System.out.println(jo.getAsJsonObject("error").get("message").getAsString());
            return null;
        }
        
        List<JsonObject> playlists = new ArrayList<>();
        List<String> results = new ArrayList<>();
        jo.getAsJsonObject("playlists")
                .getAsJsonArray("items")
                .forEach(je -> playlists.add(je.getAsJsonObject()));
        
        for (JsonObject playlist : playlists) {
            String name = playlist.get("name").getAsString();
            String url = playlist.getAsJsonObject("external_urls").get("spotify").getAsString();
            results.add(name + "\n" + url + "\n");
        }
        return results;
    }
    
    private void fillCategoriesCache() throws IOException, InterruptedException {
        JsonObject jo = getRequest("/v1/browse/categories");
        if (jo.has("error")) {
            System.out.println(jo.getAsJsonObject("error").get("message").getAsString());
            return;
        }
        List<JsonObject> categories = new ArrayList<>();
        jo.getAsJsonObject("categories")
                .getAsJsonArray("items")
                .forEach(je -> categories.add(je.getAsJsonObject()));
        
        cachedCategoriesToIds.clear();
        for (JsonObject category : categories) {
            String name = category.get("name").getAsString();
            String id = category.get("id").getAsString();
            cachedCategoriesToIds.put(name.trim(), id);
        }
    }
    
    List<String> getCategories() {
        try {
            fillCategoriesCache();
            return new ArrayList<>(cachedCategoriesToIds.keySet());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    List<String> getPlaylists(String categoryName) {
        try {
            if (cachedCategoriesToIds.isEmpty()) {
                fillCategoriesCache();
            }
            String categoryId = cachedCategoriesToIds.get(categoryName.trim());
            if (categoryId == null) {
                System.out.println("Unknown category name.");
                return null;
            }
            
            JsonObject jo = getRequest("/v1/browse/categories/" + categoryId + "/playlists");
            return parsePlaylists(jo);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}


class AuthClient {
    private String tokenUri;
    private String authorizeUri;
    
    private static String client_id = "9792bed9428a40a1b79411f854062720";
    private static String client_secret = "48d899ccb2b144dfa1802ca2c7350557";
    
    private static String redirect_uri = "http://localhost:8080/callback";
    
    
    public AuthClient(String spotifySite) {
        tokenUri = spotifySite + "/api/token";
        authorizeUri = spotifySite + "/authorize?client_id="
                + client_id + "&response_type=code&redirect_uri=" + redirect_uri;
    }
    
    void askAuthorization() {
        System.out.println("use this link to request the access code:\n" + authorizeUri);
    }
    
    String requestToken(String authorizationCode) throws Exception {
        String body = "grant_type=authorization_code"
                + "&code=" + authorizationCode
                + "&redirect_uri=" + redirect_uri
                + "&client_id=" + client_id
                + "&client_secret=" + client_secret;
        
        HttpClient client = HttpClient.newBuilder().build();
        HttpRequest request = HttpRequest.newBuilder()
                .header("Content-Type", "application/x-www-form-urlencoded")
                .uri(URI.create(tokenUri))
                .POST(HttpRequest.BodyPublishers.ofString(body))
                .build();
        
        
        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
        if (!response.body().contains("access_token")) {
            throw new IllegalStateException("Not found access token: " + response.body());
        }
        
        JsonObject jo = JsonParser.parseString(response.body()).getAsJsonObject();
        String accessToken = jo.get("access_token").getAsString();
        return accessToken;
    }
    
}
