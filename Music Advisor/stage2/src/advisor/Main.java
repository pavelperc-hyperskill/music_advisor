// user solution:
// https://stepik.org/submissions/625272/140627124
package advisor;

import java.util.Scanner;

public class Main {
    static boolean isAuthorized = false;
    static final String AUTH_LINK = "https://accounts.spotify.com/authorize?client_id=54a5f7d3b3a644b18256ff82c0dbbc64&redirect_uri=https://www.example.com&response_type=code";
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (true){
            String command = scanner.nextLine();
            if (command.equals("auth")){
                System.out.println(AUTH_LINK);
                System.out.println("---SUCCESS---");
                isAuthorized = true;
            } else if (command.equals("exit")){
                System.out.println("---GOODBYE!---");
                break;
            } else if (isAuthorized) {
                if (command.equals("new")){
                    System.out.println("---NEW RELEASES---");
                } else if (command.equals("featured")) {
                    System.out.println("---FEATURED---");
                } else if (command.equals("categories")){
                    System.out.println("---CATEGORIES---");
                } else if (command.contains("playlists")){
                    String[] playlist = command.split(" ");
                    System.out.println(String.format("---%s PLAYLISTS---", playlist[1].toUpperCase()));
                }
            } else {
                System.out.println("Please, provide access for application.");
            }
            
        }
    }
}
